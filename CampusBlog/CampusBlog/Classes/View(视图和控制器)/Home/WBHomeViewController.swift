//
//  WBHomeViewController.swift
//  CampusBlog
//
//  Created by 吴雷 on 2017/11/11.
//  Copyright © 2017年 吴雷. All rights reserved.
//

import UIKit

// 定义全局常量，尽量用 private 修饰，否则到处可以访问
private let cellId = "cellId"

class WBHomeViewController: WBBaseViewController {

    ///微博数据数组
    private lazy var statusList = [String]()
    
    /// 加载数据
    override func loadData() {
        
        // 用网络工具加载微博数据
        WBNetworkManager.shared.statusList { (list, isSuccess) in
            
            //字典转模型， 绑定表格数据
            print(list)
        }
        
        print("开始加载数据 \(WBNetworkManager.shared)")
        
        // 模拟‘延时’加载数据 -> dispatch_after
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            
        for i in 0..<15 {
            if self.isPullup {
                // 将数据追加到底部
                self.statusList.append("上拉 \(i)")
            } else {
                // 将数据插入到数组的顶部
                self.statusList.insert(i.description, at: 0)
            }
            
        }
        
        print("加载数据结束")
        // 结束刷新控件
        self.refreshControl?.endRefreshing()
        // 恢复上拉刷新标记
        self.isPullup = false
            
        // 刷新表格
        self.tableView?.reloadData()
    }
}
    
    ///显示好友
    @objc private func showFriends() {
        
        let vc = WBDemoViewController()
        
        // vc.hidesBottomBarWhenPushed =  true
        
        // push 的动作是 nav 做的
        
        navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - 表格数据源方法,具体的数据源方法实现，不需要 super
extension WBHomeViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statusList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // 1. 取 cell
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath )
        
        // 2. 设置 cell
        cell.textLabel?.text = statusList[indexPath.row]
        
        // 3. 返回 cell
        return cell
    }
}

// MARK: - 设置界面

extension WBHomeViewController {
    
    /// 重写父类方法
       @objc override func setupTableView() {

        super.setupTableView()
        
        /// 设置导航栏按钮
        /// 无法高亮 --重写navigation前代码
        // navigationItem.leftBarButtonItem = UIBarButtonItem(title: "好友", style: .plain, target: self, action: #selector(showFriends))
        
        // Swift 调用 OC 返回 instancetype 的方法，判断不出是否可选
        //        let btn: UIButton = UIButton.cz_textButton("好友", fontSize: 16, normalColor: UIColor.darkGray, highlightedColor: UIColor.orange)
        //        btn.addTarget(self, action: #selector(showFriends), for: .touchUpInside)
        //
        //        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btn)
        navItem.leftBarButtonItem = UIBarButtonItem(title: "好友", target: self, action: #selector(showFriends))
        
        // 注册原型 cell
        tableView?.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        
  }
}
