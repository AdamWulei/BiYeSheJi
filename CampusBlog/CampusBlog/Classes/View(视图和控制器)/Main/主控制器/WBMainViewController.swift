//
//  WBMainViewController.swift
//  CampusBlog
//
//  Created by 吴雷 on 2017/11/11.
//  Copyright © 2017年 吴雷. All rights reserved.
//

import UIKit

/// 主控制器
class WBMainViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupChildControllers()
        setupComposeButtom()
    }
    
    /**
        portrait    : 竖屏，肖像
        lanscape    : 横屏，风景
     
        - 使用代码控制设备方向，好处：可以在需要横屏的时候，单独处理
        - 设置支持的方向之后，当前的控制器及子控制器都会遵守这个方向
        - 如果播放视频，通常通过 modal 展现
    */
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    // MARK: - 监听方法
    /// 撰写微博
    // FIXME: 没有实现
    // private 私有，仅在当前对象被访问
    // @objc 允许这个函数在‘运行时’通过 OC 的消息机制被调用
    @objc private func composeStatus() {
        
        print("撰写微博")
        
        // 测试方向旋转
        let vc = UIViewController()
        
        vc.view.backgroundColor = UIColor.cz_random()
        let nav = UINavigationController(rootViewController: vc)
        
        present(nav, animated: true, completion: nil)
    }
    
    
    // MARK: - 私有控件
    /// 撰写按钮
     private lazy var composeButton: UIButton = UIButton.cz_imageButton(
        "tabbar_compose_icon_add",
        backgroundImageName: "tabbar_compose_button")
}

// extension 类似于OC中的分类，在Swift中还可以用来切分类代码块
// 可以把相近功能的函数，放在一个extension中
// 便于代码维护
// 注意：和OC的分类一样，extension中不能定义属性
// MARK: - 设置界面
extension WBMainViewController{
    
    /// 设置撰写按钮
    private func setupComposeButtom() {
        tabBar.addSubview(composeButton)
        
        // 计算按钮的宽度
        let count = CGFloat(childViewControllers.count)
        // 将向内缩进的宽度减少，能够让按钮的宽度变大，盖住容错点
        let w = tabBar.bounds.width / count - 1
        
        // CGRectInset 正数向内缩进，负数向外扩展
        composeButton.frame = tabBar.bounds.insetBy(dx: 2 * w, dy: 0)
        
        // 按钮监听方法
        composeButton.addTarget(self, action: #selector(composeStatus), for: .touchUpInside)
        
    }
    
    /// 设置所有子控制器
    private func setupChildControllers() {
        
        //在现在的很多引用程序中，界面的创建都依赖网络的 json
        let array: [[String: Any]] = [
            ["clsName": "WBHomeViewController", "title": "首页", "imageName": "home",
             "visitorInfo": ["imageName": "", "message": "关注一些人，回这里看看有什么惊喜"]
            ],
            ["clsName": "WBMessageViewController", "title": "消息", "imageName": "message_center",
             "visitorInfo": ["imageName": "visitordiscover_image_message", "message": "登录后，别人评论你的微博，发给你的消息，都会在这里收到通知"]
             ],
            ["clsName": "UIviewcontroller"],
            ["clsName": "WBDiscoverViewController", "title": "发现", "imageName": "discover",
             "visitorInfo": ["imageName": "visitordiscover_image_message", "message": "登录后，最新、最热微博尽在掌握，不再会与实事潮流擦肩而过"]
             ],
            ["clsName": "WBProfileViewController", "title": "我", "imageName": "profile",
             "visitorInfo": ["imageName": "visitordiscover_image_profile", "message": "登陆后，你的微博、相册、个人资料会显示在这里，展示给别人"]
             ]
        ]
        
        // 测试数据格式是否正确 - 转化成 plist 数据更加直观
        // (array as NSArray).write(toFile: "/Users/mac/Desktop/demo.plist", atomically: true)
        // 数组 -> Json 序列化
        

        var arrayM = [UIViewController]()
        for dict in array {

            arrayM.append(controller(dict: dict as [String : AnyObject]))
        }

        viewControllers = arrayM
    }
    
    /// 使用字典创建一个子控制器
    ///
    /// - Parameter dict: 信息字典[clsName ,title, imageName,visitorInfo]
    /// - Returns: 子控制器
    private func controller(dict: [String: AnyObject]) -> UIViewController {

        //1. 取得字典内容
        guard let clsName = dict["clsName"] as? String,
              let title = dict["title"] as? String,
              let imageName = dict["imageName"] as? String,
              let cls = NSClassFromString(Bundle.main.namespace + "." + clsName) as? WBBaseViewController.Type,
              let visitorDict = dict ["visitorInfo"] as? [String: String]
        else  {

                return UIViewController()
        }
        //2. 创建视图控制器

        let vc = cls.init()

        vc.title = title
        
        // 设置控制器的访客信息字典
        vc.visitorInfoDictionary = visitorDict

        //3. 设置图像
        vc.tabBarItem.image = UIImage(named: "tabbar_" + imageName)
        vc.tabBarItem.selectedImage = UIImage(named: "tabbar_" + imageName + "_selected")?.withRenderingMode(.alwaysOriginal)
        
        //4. 设置 tabbar 的标题字体
        // NSAttributedStringKey const NSForegroundColorAttributeName
        vc.tabBarItem.setTitleTextAttributes(
            [NSAttributedStringKey.foregroundColor: UIColor.orange],
            for: .highlighted)
        // 系统默认是12号字体。修改字体大小，要设置 Normal 的数值
        vc.tabBarItem.setTitleTextAttributes(
            [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12)],
            for: UIControlState(rawValue: 0))
        
        // 实例化导航控制器的时候， 会调用push方法将 rootVC 压栈
        let nav = WBNavigationController(rootViewController: vc)

        return nav

    }
    
}

    
    
    
    
    
    
    
    
    
    
    

