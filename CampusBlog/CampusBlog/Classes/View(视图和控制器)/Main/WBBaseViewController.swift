//
//  WBBaseViewController.swift
//  CampusBlog
//
//  Created by 吴雷 on 2017/11/11.
//  Copyright © 2017年 吴雷. All rights reserved.
//

import UIKit

// 面试题： OC中支持多继承吗？ 如果不支持，如何替代？ 使用协议替代
// Swift 的写法类似于多继承
// class WBBaseViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
// Swift 中，利用 extension 可以把‘函数’按照功能分类管理，便于阅读和维护
// 注意：
// 1. extension 中不能有属性
// 2. extension 中不能重写父类方法，重写父类方法是子类的职责，扩展是对类的扩展

/// 所有主控制器的基类控制器
class WBBaseViewController: UIViewController {
    
    /// 用户登陆标记
    var userLogon = true
    
    /// 访客视图信息字典
    var visitorInfoDictionary: [String: String]?
    
    /// 表格视图 - 如果用户没有登陆，就不创建
    var tableView: UITableView?
    /// 刷新控件
    var refreshControl: UIRefreshControl?
    /// 上拉刷新标记
    var isPullup = false
    
    /// 自定义导航条 UIScreen.cz_screenWidth()
    lazy var navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 12, width: UIScreen.cz_screenWidth(), height: 64))
    
    /// 自定义的导航条目 - 以后设置导航栏内容，统一使用 navItem
    lazy var navItem = UINavigationItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        //view.backgroundColor = UIColor.cz_random()
        
        loadData()
    }
    
    /// 重写 title 的 didset
    override var title: String?{
        didSet {
            navItem.title = title
        }
    }
    
    /// 加载数据 - 具体的实现由子类负责
    @objc func loadData() {
        // 如果子类不实现任何方法，默认关闭刷新控件
        refreshControl?.endRefreshing()
    }
}

// MARK: - 访客视图监听方法
extension WBBaseViewController {
    
    @objc private func login() {
        print("用户登录")
    }
    
    @objc private func register(){
        print("用户注册")
    }
}

// MARK: - 设置界面
extension WBBaseViewController {
    
   @objc private func setupUI() {
        view.backgroundColor = UIColor.white
    
        //取消自动缩进 - 如果隐藏了导航栏，会缩进 20 个点
        automaticallyAdjustsScrollViewInsets = false
    
        setupNavigationBar()
    
        userLogon ? setupTableView() : setupVisitorView()
    
    }
    
    /// 设置表格视图 - 用户登录之后执行
    /// 子类重写此方法，因为子类不需要关心用户登录之前的逻辑
     @objc func setupTableView() {
        
        tableView = UITableView(frame:  view.bounds, style: .plain)
        
        view.insertSubview(tableView!, belowSubview: navigationBar)
        
        // 设置数据源和代理 -> 目的： 子类直接实现数据源方法
        tableView?.dataSource = self
        tableView?.delegate = self
        
        // 设置内容缩进 navigationbar.bounds.height 35 10
        tableView?.contentInset = UIEdgeInsets(top: 35, left: 0, bottom: 0, right: 0)
        
        // 设置刷新空间
        // 1> 实例化空间
        refreshControl = UIRefreshControl()
        
        // 2> 添加到表格视图
        tableView?.addSubview(refreshControl!)
        
        // 3> 添加监听方法
        refreshControl?.addTarget(self, action: #selector(loadData), for: .valueChanged)
    }
    
    /// 设置访客视图
    private func setupVisitorView() {
        
        let visitorView = WBVisitorView(frame: view.bounds)
                
        view.insertSubview(visitorView, belowSubview: navigationBar)
        
        // print("访客视图 \(visitorView)")
        
        // 1. 设置访客视图信息
        visitorView.visitorInfo = visitorInfoDictionary
        
        // 2. 添加访客视图按钮的监听方法
        visitorView.loginButton.addTarget(self, action: #selector(login), for: .touchUpInside)
        visitorView.reqisterButton.addTarget(self, action: #selector(register), for: .touchUpInside)
        
        // 3. 设置导航条按钮
        navItem.leftBarButtonItem = UIBarButtonItem(title: "注册", style: .plain, target: self, action: #selector(register))
        navItem.rightBarButtonItem = UIBarButtonItem(title: "登录", style: .plain, target: self, action: #selector(login))
        
    }
    
    
    ///设置导航条
    private func setupNavigationBar() {
        // 添加导航条
        view.addSubview(navigationBar)
        // 将 item 设置给 bar
        navigationBar.items = [navItem]
        // 1> 设置 navbar 的渲染颜色
        navigationBar.barTintColor = UIColor.cz_color(withHex: 0xF6F6F6)
        // 2> 设置 navbar 的字体颜色
        navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.darkGray]
        // 3> 设置系统按钮的文字渲染颜色
        navigationBar.tintColor = UIColor.orange
        
        
        // navigationBar.bounds.height = navigationBar.bounds.height + 10
        
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension WBBaseViewController: UITableViewDataSource, UITableViewDelegate {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    // 基类只是准备方法，子类负责具体的实现
    // 子类的数据源不需要 super
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // 只是保证没有语法错误
        return UITableViewCell()
    }
    
    /// 在显示最后一行的时候做上拉刷新
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // 1. 判断 indexpath 是否最后一行
        // (indexPath.section(最大) / indexPath.row(最后一行))
        // 1> row
        let row = indexPath.row
        // 2> section
        let section = tableView.numberOfSections - 1
        
        if row < 0 || section < 0 {
            return
        }
        
        // 3> 行数
        let count = tableView.numberOfRows(inSection: section)
        
        // 如果是最后一行，同时没有开始上拉刷新
        if row == (count - 1) && !isPullup {
            
            print("上拉刷新")
            isPullup = true
            
            // 开始刷新
            loadData()
        }
        
    }
}


