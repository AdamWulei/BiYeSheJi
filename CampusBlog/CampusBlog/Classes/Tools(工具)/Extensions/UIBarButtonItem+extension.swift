//
//  UIBarButtonItem+extension.swift
//  CampusBlog
//
//  Created by 吴雷 on 2017/11/17.
//  Copyright © 2017年 吴雷. All rights reserved.
//

import UIKit

/// 创建 UIBarButtonItem
extension UIBarButtonItem {
    
    convenience init(title: String, fontSize: CGFloat = 16, target: AnyObject?, action: Selector, isbackButton: Bool = false) {
        let btn: UIButton = UIButton.cz_textButton(title, fontSize: fontSize, normalColor: UIColor.darkGray, highlightedColor: UIColor.orange)
        
        if isbackButton {
            let imageName = "navigationbar_back_withtext"
            btn.setImage(UIImage(named: imageName), for: UIControlState(rawValue: 0))
            btn.setImage(UIImage(named: imageName + "_highlighted"), for: .highlighted )
            
            btn.sizeToFit()
        }
        
       
        btn.addTarget(target, action: action, for: .touchUpInside)
        
        // self.init 实例化 UIBarButtonItem
        self.init(customView: btn)
    }
}
