//
//  WBNetworkManager+Extension.swift
//  CampusBlog
//
//  Created by 吴雷 on 2017/11/22.
//  Copyright © 2017年 吴雷. All rights reserved.
//

import Foundation

// MARK: - 封装微博的网络请求方法
extension WBNetworkManager {
    
    /// 加载微博数据字典
    ///
    /// - Parameter completion: 完成回调[list: 微博字典数组/是否成功]
    func statusList (completion:@escaping (_ list: [[String: Any]], _ isSuccess: Bool)->()) {
        
        let urlSting = "https://api.weibo.com/2/statuses/home_timeline.json"
        // let params = ["access_token": "2.00c7xgwC84e1pC6fc097eaba0Buj67"]
        
        tokenRequest(URLString: urlSting, parameters: nil) { (json, isSuccess) in
            
            let jsonArray = json as? [String: Any]
            
            let result = jsonArray?["statuses"] as? [[String: Any]]
            
           // completion(result ?? [], isSuccess)
            
            completion(result!, true)
        }
    }
}
