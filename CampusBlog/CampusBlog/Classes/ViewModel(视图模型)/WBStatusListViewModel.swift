//
//  WBStatusListViewModel.swift
//  CampusBlog
//
//  Created by 吴雷 on 2017/11/25.
//  Copyright © 2017年 吴雷. All rights reserved.
//

import Foundation

//如果没有任何父类，要打印类的信息，需要遵守CustomStringConvertible 协议，实现description方法
class WBStatusListViewModel {
    
    lazy var statusList = [WBStatus]()
    
    func loadStatus(completion: @escaping (_ isSuccess: Bool)->()) {
        
        WBNetworkManager.shared.statusList { (list, isSuccess) in
            
            guard let array = NSArray.yy_modelArray(with: WBStatus.self, json: list ) as? [WBStatus] else {
                
                completion(isSuccess)
                return
            }
            
            self.statusList += array
            
            completion(isSuccess)
           
        }
    }
}
    
    

