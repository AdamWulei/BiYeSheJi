//
//  WBStatus.swift
//  CampusBlog
//
//  Created by 吴雷 on 2017/11/25.
//  Copyright © 2017年 吴雷. All rights reserved.
//

import UIKit
import YYModel
/// 微博数据模型
class WBStatus: NSObject {
    
    var id: Int64 = 0
    
    var text: String?
    
    var reposts_count: Int = 0
    
    var comments_count: Int = 0
    
    var attitudes_count: Int = 0
    
    //被转发的微博
//    var retweeted_status: WBStatusModel?
//
//    var user: WBUser?
//
//    var pic_urls: [WBPictureModel]?
    
    override var description: String {
        
        return yy_modelDescription()
    }
    
//    class func modelContainerPropertyGenericClass() -> [String: AnyClass] {
//        
//        return ["pic_urls": WBPictureModel.self]
//        
//    }
    
    
}
