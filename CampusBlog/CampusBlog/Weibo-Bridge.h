//
//  Weibo-Bridge.h
//  CampusBlog
//
//  Created by 吴雷 on 2017/11/12.
//  Copyright © 2017年 吴雷. All rights reserved.
//

//桥接文件，专门用于引入OC的头文件，一旦引入，Swift就可以正常使用（宏除外）

#ifndef Weibo_Bridge_h
#define Weibo_Bridge_h

#import "CZAddition.h"
#import "UIButton.h"
#import "UIScreen.h"
#import "UILabel.h"

#endif /* Weibo_Bridge_h */
